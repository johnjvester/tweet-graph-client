# `tweet-graph-client`

> The `tweet-graph-client` repository contains an [Angular](https://angular.io/) application, utilizing 
> [ngx-graph](https://swimlane.github.io/ngx-graph/) to present data stored in [Draph Slash GraphQL](https://dgraph.io/graphql), 
> which stems from actual data in [Twitter](https://twitter.com/home).  There is also an `import` folder which 
> contains Twitter integration logic written in [Python](https://www.python.org/) and leveraging the 
> [twarc](https://github.com/DocNow/twarc) command-line tool and Python library or archiving Twitter data.

The following illustration represents the flow which is accomplished with the `tweet-graph-client` repository:

![Twitter Post](./images/SimpleFlow.png)

## Publications

This repository is related to an article published on DZone.com:

* [Creating a Twitter Graph Using Slash GraphQL](https://dzone.com/articles/creating-a-twitter-graph-using-slash-graphql)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939

## Preparing Slash GraphQL

Within Slash GraphQL, create a new backend instance (e.g. `tweet-graph`) and note the **GraphQL Endpoint** value in 
the [Overview](https://slash.dgraph.io/_/dashboard) screen:

![Slash GraphQL Overview](./images/SlashGraphQLOverview.png)

Set this value to the `TWEET_GRAPH_API` system variable:

```
export TWEET_GRAPH_API=https://some_unique_host_goes_here.aws.cloud.dgraph.io/graphql
```

_Please note: make sure to replace `some_unique_host_goes_here` with the value provided in **Slash GraphQL Overview** screen._

Additionally, because of the simplicity of this project, set the search string for the Twitter query (which will use `twarc`) 
in the `TWARC_SEARCH_STRING` system variable:

```
export TWARC_SEARCH_STRING=#NeilPeart
```

_Please note: as an example, this project will search for tweets which include the `#NeilPeart` hashtag, 
as an honor to the recently-departed musical genius, [Neil Peart](https://en.wikipedia.org/wiki/Neil_Peart)._

Create the following schema:

```
type User {
  handle: String! @id @search(by: [hash])
  screen_name: String! @search(by: [fulltext])
}

type Tweet {
  tweet_id: String! @id @search(by: [hash])
  content: String!
  user: User
}

type Configuration {
  id: ID
  search_string: String!
}

```

## Loading Twitter Data into Slash GraphQL

Using the code located in the `import` folder, execute the following steps:

1. install the necessary requirements, noted in the `requirements.txt` file
1. validate the `TWEET_GRAPH_API` and `TWARC_SEARCH_STRING` system variables have been set (i.e. `env` terminal command)
1. switch to the `import` folder of the repository   
1. if exists, remove the `import/tweets.jsonl` and/or `import/tweets.rdf` files
1. utilize `twarc` to perform a search `twarc search $TWARC_SEARCH_STRING > tweets.jsonl`
1. once finished, validate the `tweets.jsonl` file exists and contains valid data
1. execute the `python convert.py` command
1. execute the `python upload.py` command (this step can take a great deal of time to finish, based upon the size of the `tweets.jsonl` file)

To validate the data has been uploaded to Dgraph Slash GraphQL, run the following queries from the API Explorer:

```
query MyQuery {
  queryTweet {
    content
    tweet_id
    user {
      screen_name
      handle
    }
  }
}

query MyQuery {
  queryConfiguration {
    search_string
  }
}
```

### Removing data from Slash GraphQL

Due to the basic design of this application, only one `Configuration` item is expected.  It is important to remove data from 
Slash GraphQL before running the import an additional time.  Please run the following queries from **API Explorer** in order to 
clear out the Slash GraphQL instance:

```
mutation MyMutation {
  deleteTweet(filter: {}) {
    msg
    numUids
  }
  deleteUser(filter: {}) {
    msg
    numUids
  }
  deleteConfiguration(filter: {}) {
    msg
    numUids
  }
}
```

## Running the `tweet-graph-client` Application

Before starting the Angular application, the `environment.ts` file(s) need to include the `api` URI for the Draph Slash GraphQL service.

```
export const environment = {
  api: 'https://some_unique_host_goes_here.us-west-2.aws.cloud.dgraph.io/graphql',
  ...
};
```

_Please note: make sure to replace `some_unique_host_goes_here` with the value provided in **Slash GraphQL Overview** screen._

Start the Angular client using the following command:

`ng serve`

_Please note: for additional information, please review the [Angular CLI](https://angular.io/cli/serve) options._

Once started, the Angular client will retrieve data from the Slash GraphQL and present a user-experience similar to what is displayed below:

![Angular Client - #NeilPeart Activity](./images/NeilPeartTweets.png)

Single-clicking a node will open the actual item (user home screen or tweet) directly from Twitter.  Below is a link to a tweet-based node:

![Twitter Post](./images/ActualTwitterData.png)

### About `ngx-graph`

The current implementation of `ngx-graph` is using the following properties:

* `dagre` layout
* custom templates (line, node, edge)
* including `(click)` functionality


Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
