import json
import os

from graphqlclient import GraphQLClient

client = GraphQLClient(os.getenv('TWEET_GRAPH_API'))

data = open('tweets.jsonl').read()
json_data = [json.loads(jline) for jline in data.splitlines()]


def upload_to_slash(query):
    response = json.loads(client.execute(query))
    print(repr(response))
    try:
        if response['data']:
            print("Uploaded")
        else:
            print("Error: {}".format(response))
    except KeyError:
        print(query)

    return response


def sanitise(content):
    return json.dumps(content.replace('"', '\\"').replace('\n', ' '))


def tweet_fragment(user, tweet):
    return '''
     addTweet(input: {{ tweet_id: {}, content: {}, user: {{ handle: {} }} }}) {{
        tweet {{
          tweet_id,
          content
        }}
      }}
    '''.format(sanitise(tweet['id']), sanitise(tweet['content']), sanitise(user['handle']))


def create_add_tweets_query(user, tweets):
    query = '''
    mutation addTweets {{
      addUser(input: {{handle: {}, screen_name: {} }}) {{
        user {{
          handle
        }}
      }}
    '''.format(sanitise(user['handle']), sanitise(user['screen_name']))

    for tweet in tweets:
        query += tweet_fragment(user, tweet)
        query += '}'

    return query


def create_configuration_query(search_string):
    query = '''
    mutation addConfiguration {{
      addConfiguration(input: {{ search_string: {} }}) {{
        configuration {{
          search_string
        }}
      }}
    }}
    '''.format(search_string)

    return query


def gather_tweets_by_user():
    for tweet in json_data:
        handle = tweet['user']['screen_name']
        screen_name = tweet['user']['name']
        content_ = tweet['full_text']
        tweet_id = tweet['id_str']

        user = {"handle": handle, "screen_name": screen_name}

        users[screen_name] = user

        tweet = {"id": tweet_id, "content": content_}

        user_tweets = [tweet]

        if screen_name in data:
            tweets = data[screen_name]
            tweets += user_tweets
            data[screen_name] = tweets
        else:
            data[screen_name] = user_tweets


data = {}
users = {}

gather_tweets_by_user()

search_string = os.getenv('TWARC_SEARCH_STRING')
print(search_string)

upload_to_slash(create_configuration_query(search_string))

for handle in data:
    print("=====")
    upload_to_slash(create_add_tweets_query(users[handle], data[handle]))
