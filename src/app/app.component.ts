import {Component, OnInit} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {Subject} from 'rxjs';
import {ConversionService} from './services/conversion.service';
import {GraphPayload} from './models/graph-payload';
import {GraphNode} from './models/graph-node';
import {GraphQlService} from './services/graph-ql.service';
import {QueryResult} from './models/query-result';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'tweet-graph-client';
  filterValue: string;
  showData: boolean = false;
  zoomToFit$: Subject<boolean> = new Subject();
  graphPayload: GraphPayload;

  constructor(private spinner: NgxSpinnerService, private conversionService: ConversionService, private graphQlService: GraphQlService) {}

  ngOnInit() {
    this.spinner.show();

    this.graphQlService.getConfigurationData().subscribe(configs => {
      if (configs) {
        this.filterValue = configs.data.queryConfiguration[0].search_string;

        this.graphQlService.getTweetData().subscribe(data => {
          if (data) {
            let queryResult: QueryResult = data;
            this.graphPayload = this.conversionService.createGraphPayload(queryResult);
            this.fitGraph();
            this.showData = true;
          }
        }, (error) => {
          console.error('error', error);
        }).add(() => {
          this.spinner.hide();
        });
      }
    }, (error) => {
      console.error('error', error);
    }).add(() => {
      this.spinner.hide();
    });
  }

  fitGraph() {
    this.zoomToFit$.next(true)
  }

  clickNode(node:GraphNode) {
    window.open('https://twitter.com/' + node.twitter_uri, '_BLANK');
  }
}
