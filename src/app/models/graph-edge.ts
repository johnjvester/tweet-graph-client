import {Edge} from '@swimlane/ngx-graph';

export class GraphEdge implements Edge {
  id: string;
  label: string;
  source: string;
  target: string;
}
