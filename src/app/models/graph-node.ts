import {Node} from '@swimlane/ngx-graph';

export class GraphNode implements Node {
  id: string;
  label: string;
  twitter_uri: string;
}
