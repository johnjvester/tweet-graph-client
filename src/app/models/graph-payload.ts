import {GraphNode} from './graph-node';
import {GraphEdge} from './graph-edge';

export class GraphPayload {
  links: GraphEdge[] = [];
  nodes: GraphNode[] = [];
}
