import {QueryConfigurations} from './query-configurations';

export class QueryConfigResult {
  data: QueryConfigurations;
  extensions: any;
}
