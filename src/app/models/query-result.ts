import {QueryData} from './query-data';

export class QueryResult {
  data: QueryData;
  extensions: any;
}
