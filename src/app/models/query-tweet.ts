import {User} from './user';

export class QueryTweet {
  content: string;
  tweet_id: string;
  user: User;
}
