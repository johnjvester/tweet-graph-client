import { Injectable } from '@angular/core';
import {GraphPayload} from '../models/graph-payload';
import {QueryResult} from '../models/query-result';
import {QueryTweet} from '../models/query-tweet';
import {GraphNode} from '../models/graph-node';
import {GraphEdge} from '../models/graph-edge';

@Injectable({
  providedIn: 'root'
})
export class ConversionService {

  constructor() { }

  createGraphPayload(queryResult: QueryResult): GraphPayload {
    let graphPayload: GraphPayload = new GraphPayload();

    if (queryResult) {
      if (queryResult.data && queryResult.data.queryTweet) {
        let tweetList: QueryTweet[] = queryResult.data.queryTweet;

        tweetList.forEach( (queryTweet) => {
          let tweetNode:GraphNode = this.getTweetNode(queryTweet, graphPayload);
          let userNode:GraphNode = this.getUserNode(queryTweet, graphPayload);

          if (tweetNode && userNode) {
            let graphEdge:GraphEdge = new GraphEdge();
            graphEdge.id = ConversionService.createRandomId();

            if (tweetNode.label.substring(0, 2) === 'RT') {
              graphEdge.label = 'retweet';
            } else {
              graphEdge.label = 'tweet';
            }

            graphEdge.source = userNode.id;
            graphEdge.target = tweetNode.id;
            graphPayload.links.push(graphEdge);
          }
        });
      }
    }

    console.log('graphPayload', graphPayload);
    return graphPayload;
  }

  private getTweetNode(queryTweet: QueryTweet, graphPayload: GraphPayload):GraphNode {
    if (!graphPayload.nodes || !graphPayload.nodes.find(q => q.label === queryTweet.content)) {
      let thisNode:GraphNode = new GraphNode();
      thisNode.id = ConversionService.createRandomId();
      thisNode.label = queryTweet.content;
      thisNode.twitter_uri = queryTweet.user.handle + '/status/' + queryTweet.tweet_id;
      graphPayload.nodes.push(thisNode);
      return thisNode;
    } else {
      return graphPayload.nodes.find(q => q.label === queryTweet.content);
    }
  }

  private getUserNode(queryTweet: QueryTweet, graphPayload: GraphPayload):GraphNode {
    if (!graphPayload.nodes || !graphPayload.nodes.find(q => q.label === queryTweet.user.handle)) {
      let thisNode:GraphNode = new GraphNode();
      thisNode.id = ConversionService.createRandomId();
      thisNode.label = queryTweet.user.screen_name;
      thisNode.twitter_uri = queryTweet.user.handle;
      graphPayload.nodes.push(thisNode);
      return thisNode;
    } else {
      return graphPayload.nodes.find(q => q.label === queryTweet.user.handle);
    }
  }

  private static createRandomId(): string {
    let result: string = '';
    const characters: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    const charactersLength: number = characters.length;

    for (let i = 0; i < 9; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
  }
}
