import { Injectable } from '@angular/core';
import {QueryResult} from '../models/query-result';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, tap} from 'rxjs/operators';
import ErrorUtils from '../utils/error-utils';
import {QueryConfigResult} from '../models/query-config-result';

@Injectable({
  providedIn: 'root'
})
export class GraphQlService {
  allTweets:string = 'query MyQuery {' +
    '  queryTweet {' +
    '    content' +
    '    tweet_id' +
    '    user {' +
    '      screen_name' +
    '      handle' +
    '    }' +
    '  }' +
    '}';

  configuration:string = 'query MyQuery {' +
    '  queryConfiguration {' +
    '    search_string' +
    '  }' +
    '}';

  constructor(private http: HttpClient) { }

  baseUrl: string = environment.api;

  getTweetData() {
    return this.http.get<QueryResult>(this.baseUrl + '?query=' + this.allTweets).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }

  getConfigurationData() {
    return this.http.get<QueryConfigResult>(this.baseUrl + '?query=' + this.configuration).pipe(
      tap(),
      catchError(err => { return ErrorUtils.errorHandler(err)
      }));
  }
}
