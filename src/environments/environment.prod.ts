export const environment = {
  api: 'https://some_unique_host_goes_here.us-west-2.aws.cloud.dgraph.io/graphql',
  production: true
};
